import java.util.Arrays;
import java.util.LinkedList;

public class DoubleStack {
	private LinkedList<Double> stack;

	public static void main(String[] argum) {
		System.out.println(interpret("5 1 - 7 * 6 3 / +"));
		System.out.println(interpret("5 1 - 7 ^"));
	}

	DoubleStack() {
		stack = new LinkedList<Double>();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		DoubleStack temp = new DoubleStack();
		if (stack.size() > 0) {
			for (int i = 0; i < stack.size(); i++) {
				temp.stack.addLast(stack.get(i));
			}

		}

		return temp;

	}

	public boolean stEmpty() {

		return stack.size() == 0; 
	}

	public void push(double a) {
		stack.addLast(a);
	}

	public double pop() {
		if (stEmpty()) {
			throw new IndexOutOfBoundsException("Stack underflow: stack:" + Arrays.asList(stack));

		}
		return stack.removeLast(); // TODO!!! Your code here!
	} // pop

	public void op(String s) {
		if (stack.size() < 2) {
			throw new RuntimeException("Stack Underflow: stack size:" + stack.size() + "stack:" + Arrays.asList(stack));
		}
		double op2 = pop();
		double op1 = pop();

		if (s.equals("+")) {
			push(op1 + op2);

		} else if (s.equals("-")) {
			push(op1 - op2);

		} else if (s.equals("*")) {
			push(op1 * op2);

		} else if (s.equals("/")) {
			push(op1 / op2);

		} else {
			throw new RuntimeException("Wrong operator! Supported are +,-,/,*!");
		}
		// TODO!!!
	}

	public double tos() {
		if (stEmpty()) {
			throw new IndexOutOfBoundsException(
					"Stack is empty! Add elements please! Empty stack:" + Arrays.asList(stack));
		}
		return stack.getLast(); // TODO!!! Your code here!
	}

	@Override
	public boolean equals(Object o) {
		if (stack.equals(((DoubleStack) o).stack)) {
			return true;
		} else {
			return false;
		}
		// TODO!!! Your code here!
	}

	@Override
	public String toString() {
		if (stEmpty()) {
			return "Stack is empty! Stack:" + Arrays.asList(stack);
		}
		return stack.toString();
		// TODO!!! Your code here!
	}

	public static double interpret(String pol) {
		String[] myArray = pol.trim().split("\\s+");
		DoubleStack myStack = new DoubleStack();

		String ops = "+-*/";

		for (int i = 0; i < myArray.length; i++) {
			if (!ops.contains(myArray[i].trim())) {
				try {
					myStack.push(Double.parseDouble(myArray[i].trim()));
				} catch (RuntimeException e) {
					throw new RuntimeException("interpret: wrong operator: \"" + myArray[i].trim() + "\" RPN: " + pol);
				}

			} else {
				if (myStack.stack.size() < 2) {
					throw new RuntimeException(
							"interpret: stack underflow: stack: " + Arrays.asList(myStack.stack) + " RPN: " + pol);

				}
				myStack.op(myArray[i].trim());
			}
			if (myStack.stack.size() != 1) {
				throw new RuntimeException("interpret: stack size should be 1 after last operation. " + "Stack: "
						+ Arrays.asList(myStack.stack) + " RPN: " + pol);
			}

		}
		return myStack.pop(); // TODO!!! Your code here!
	}

}
